package com.android.szkolenie.szkoleniew3_facelessfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by papcio28 on 09.05.2015.
 */
public class SuccessCounterFragment extends Fragment {
    public static final String TAG = "SuccessCounterFragment";

    private int mCounter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mCounter = 0;
    }

    public void incrementCounter() {
        mCounter++;
    }

    public int getCounter(){
        return mCounter;
    }
}
