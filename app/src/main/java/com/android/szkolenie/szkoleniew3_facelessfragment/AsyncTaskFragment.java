package com.android.szkolenie.szkoleniew3_facelessfragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.security.InvalidParameterException;

/**
 * Created by papcio28 on 09.05.2015.
 */
public class AsyncTaskFragment extends Fragment {

    public static final String FRAGMENT_TAG = "AsyncTaskFragment";

    private ProgressInterface mProgressCallback;
    private ProgressAsyncTask mAsyncTask;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof ProgressInterface) {
            mProgressCallback = (ProgressInterface) activity;
        } else {
            throw new InvalidParameterException("BLAD !");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mProgressCallback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mAsyncTask != null) {
            mAsyncTask.cancel(true);
            mAsyncTask = null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Zachowuj instancje obiektu tego fragmentu przy zmianach konfiguracji
        setRetainInstance(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Rozpoczynamy liczenie do 100 w tle
        // Tylko jezeli nie ma juz aktywnego zadania
        if(mAsyncTask == null) {
            mAsyncTask = new ProgressAsyncTask(this);
            mAsyncTask.execute();
        }

    }

    public interface ProgressInterface {
        public void onProgress(int progress);
        public void onComplete(String result);
    }

    private static class ProgressAsyncTask extends AsyncTask<Void, Integer, String> {

        private AsyncTaskFragment mFragment;

        public ProgressAsyncTask(AsyncTaskFragment mFragment) {
            this.mFragment = mFragment;
        }

        @Override
        protected String doInBackground(Void... voids) {
            int licznik = 0;
            while(licznik <= 100) {
                if(isCancelled()) {
                    return "Stopped !";
                }
                publishProgress(licznik++);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "OK";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if(mFragment.mProgressCallback != null)
                mFragment.mProgressCallback.onProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(mFragment.mProgressCallback != null)
                mFragment.mProgressCallback.onComplete(s);
            mFragment = null;
        }
    }
}
