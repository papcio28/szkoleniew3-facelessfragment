package com.android.szkolenie.szkoleniew3_facelessfragment;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements AsyncTaskFragment.ProgressInterface {

    private ProgressBar mProgress;

    private Button mStart;
    private Button mStop;
    private TextView mZakonczone;

    private SuccessCounterFragment mCounterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        mStart = (Button) findViewById(R.id.start_button);
        mStop = (Button) findViewById(R.id.stop_button);
        mZakonczone = (TextView) findViewById(R.id.zakonczone_label);

        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncTaskFragment mAsyncTaskFragment = new AsyncTaskFragment();
                getSupportFragmentManager().beginTransaction()
                        .add(mAsyncTaskFragment, AsyncTaskFragment.FRAGMENT_TAG).commit();
                mStart.setEnabled(false);
                mStop.setEnabled(true);
            }
        });

        mStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgress.setProgress(0);
                mStart.setEnabled(true);    // Odblokowujemy przycisk START
                mStop.setEnabled(false);    // Blokujemy przycisk STOP

                getSupportFragmentManager().beginTransaction()
                        .remove(getSupportFragmentManager().findFragmentByTag(AsyncTaskFragment.FRAGMENT_TAG))
                        .commit();
            }
        });

        // Pobranie z FragmentManagera
        mCounterFragment =
                (SuccessCounterFragment)getSupportFragmentManager()
                        .findFragmentByTag(SuccessCounterFragment.TAG);

        // Jeżeli nie istniał w FragmentManager to utworz nowy i dodaj
        if(mCounterFragment == null) {
            mCounterFragment = new SuccessCounterFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(mCounterFragment, SuccessCounterFragment.TAG).commit();
        }

        refreshLabel();

        // Sprawdzamy czy istnieje juz fragment odtworzony
        if(getSupportFragmentManager().findFragmentByTag(AsyncTaskFragment.FRAGMENT_TAG) != null) {
            // Jezeli tak to zmieniamy stan przyciskow
            mStart.setEnabled(false);
            mStop.setEnabled(true);
        }
    }

    public void refreshLabel() {
        mZakonczone.setText(String.format("Zakończono %d razy !", mCounterFragment.getCounter()));
    }

    @Override
    public void onProgress(int progress) {
        mProgress.setProgress(progress);
    }

    @Override
    public void onComplete(String result) {
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        mProgress.setProgress(0);
        mStart.setEnabled(true);    // Odblokowujemy przycisk START
        mStop.setEnabled(false);    // Blokujemy przycisk STOP

        getSupportFragmentManager().beginTransaction()
                .remove(getSupportFragmentManager().findFragmentByTag(AsyncTaskFragment.FRAGMENT_TAG))
                .commit();

        mCounterFragment.incrementCounter();
        refreshLabel();
    }
}
